<?php 
	require_once("connection.php");
	require_once("autoload.php");
	
	$dao = new ProdutoDAO($con);

	$produto = new $_POST['tipo']($_POST['nome_prod'],  $_POST['valor_prod']);
	$produto->autor_prod = $_POST['autor_prod'];
	$produto->desc_prod = $_POST['desc_prod'];
	$produto->idioma_prod = $_POST['idioma_prod'];
	$produto->editora = $_POST['editora'];
	$produto->id_cat = $_POST['id_cat'];
	$produto->id_campus = $_POST['id_campus'];
	$produto->usado = isset($_POST['usado']) ? 1:0;
	$produto->arq = $_FILES['arq'];
	$produto->isbn = $_POST['isbn'];
	$produto->tipo = $_POST['tipo'];
	$produto->qtd = $_POST['qtd'];
	
	$origem = $produto->arq['tmp_name'];

	$destino = "uploads/{$produto->arq['name']}";

	move_uploaded_file($origem, $destino);

	$produto->arq = $destino;
		
	$deuCerto = $dao->cadastraProduto($produto);
	
	if($deuCerto == true) {
		$url = "index?msg=Produto cadastrado com sucesso!";
	} else {
		$url = "index?msg=Produto não foi cadastrado!";	
	}
	
	header("location: {$url}");
?>


