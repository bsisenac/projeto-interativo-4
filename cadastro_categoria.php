<?php 

	include("header.php");
	require_once("connection.php");
	require_once("banco_categorias.php");

	$nome = $_POST['nome'];

	$deuCerto = cadastraCategoria($con, $nome);

	if($deuCerto == true) {
		$url = "form_categoria?msg=Categoria cadastrada com sucesso!";
	} else {
		$url = "form_categoria?msg=Categoria não foi cadastrada!";	
	}
	
	header("location: {$url}");

	include("footer.php");

?>

