<?php require_once("connection.php");?>

<div class="container">	
<div class="busca col-sm-12">
<form name="frmBusca" method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>?a=buscar" >
	<div class="col-md-10">
		<label>Busca de produto pelo título: </label>
	</div>
	<div class="form-group col-sm-6">
	<div class="input-group">
	<div class="input-group-addon"><span class="glyphicon glyphicon-search"></span></div>
		<input type="text" required class="form-control" name="palavra" placeholder="digite o título do produto que deseja encontrar..."/>	
	</div>
	</div>
	<div class="col-sm-2">
	<button type="submit" required class="btn btn-primary btn-sm" value="Buscar" >Buscar</button>
	</div>
</form>
</div>
</div>

<?php
// Conexão com o banco de dados
$conn = @mysql_connect("localhost", "root", "admin") or die("Não foi possível a conexão com o Banco");
// Selecionando banco
$db = @mysql_select_db("biblioteca", $conn) or die("Não foi possível selecionar o Banco");
 
// Recuperamos a ação enviada pelo formulário
$a = $_GET['a'];
 
// Verificamos se a ação é de busca
if ($a == "buscar") {
 
	// Pegamos a palavra
	$palavra = trim($_POST['palavra']);

	$comando = "SELECT produtos.id, nome_prod, autor_prod, valor_prod, desc_prod, usado, arq, isbn, tipo, qtd, nome, nome_campus FROM produtos inner join categorias on id_cat = categorias.id inner join campus on id_campus = campus.id WHERE nome_prod LIKE '%$palavra%' ORDER BY produtos.id DESC";
 
	// Verificamos no banco de dados produtos equivalente a palavra digitada
	$sql = mysql_query($comando);
 
	// Descobrimos o total de registros encontrados
	$numRegistros = mysql_num_rows($sql);
 
	// Se houver pelo menos um registro, exibe-o
	if ($numRegistros != 0) { ?>
	<div class="container">
		<p class="alert alert-info">
			<span class="glyphicon glyphicon-info-sign"></span> 
			<?php echo $numRegistros." produto(s) com o título '".$palavra."' encontrado(s)" ?>
		</p>
		<div class="table-responsive">
			<table class="table table-hover panel panel-default">
			<thead class="well panel-body">
 			<tr>
				<th>ID</th>
				<th>Título</th>
				<th>Autor</th>
				<th>Categoria</th>
				<th>Tipo</th>
				<th>Campus</th>
				<th>Quantidade</th>
				<th>Usado</th>
				<th>Ações</th>
			</tr>		
			</thead>
		<tbody>
		<?php 
		// Exibe os produtos e suas respectivas informações 
		 while ($produto = mysql_fetch_object($sql)) {
			echo
				"<tr>
						<td>{$produto->id}</td>
						<td>{$produto->nome_prod}</td>
						<td>{$produto->autor_prod}</td>
						<td>{$produto->nome}</td>
						<td>{$produto->tipo}</td>
						<td>{$produto->nome_campus}</td>
						<td>{$produto->qtd}</td>
						<td>"?><?php if ($produto->usado == 1) { ?>
							<span class="glyphicon glyphicon-thumbs-up"></span>	
							<?php } else { ?>
							<span class="glyphicon glyphicon-thumbs-down"></span>
						<?php } ?> 
						<?php echo "</td>
						<td><a href=\"ver_produto?id={$produto->id}\">
							<button class=\"btn btn-sm btn-info\">Ver <span class=\"glyphicon glyphicon-eye-open\"></span></button>
							</a>
							<a href=\"atualiza_produto?id={$produto->id}\">
							<button class=\"btn btn-sm btn-warning\">Editar <span class=\"glyphicon glyphicon-edit\"></span></button>
							</a>
							<a href=\"deleta_produto?id={$produto->id}\">
							<button class=\"btn btn-sm btn-danger\">Excluir <span class=\"glyphicon glyphicon-trash\"></span></button>
							</a>
						</td>
					</tr>";
		} ?>
		</tbody>
</table>
<a href="lista_produtos">
		<button class="btn btn-sm btn-default">Limpar Busca</button>
	</a>
	<hr>
</div>
</div>
	<?php
	// Se não houver registros
	 } else { ?>
	 <div class="container">
	 	<p class="alert alert-info">
			<span class="glyphicon glyphicon-info-sign"></span> 
			<?php echo "Nenhum produto foi encontrado com a palavra '".$palavra."'"; ?>
		</p>
	<a href="lista_produtos">
		<button class="btn btn-sm btn-default">Limpar Busca</button>
	</a>
	<hr>
	</div>
	<?php }
}
?>
</div>