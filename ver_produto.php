<?php 
	require_once("header.php");
	require_once("connection.php");
	require_once("autoload.php");
	require_once("banco_categorias.php");
	require_once("banco_campus.php");

	$dao = new ProdutoDAO($con);
	$prod = $dao->visualizaProdutoPorId($_GET['id']);
	$url_img = 'http://biblioteca.senac.br/';
?>
	
<div class="container">
	<h3>Visualização do Poduto #<?= $prod->id ?>
	</h3>
	<div class="row">
	<div class="panel-body">
		<div class="row">
<div class="col-lg-3 thumbnail">
<?php if($prod->arq != 'uploads/') { ?>
<img src="<?= $url_img ?><?= $prod->arq ?>" class="img-responsive"/>
<?php } else { ?>
<div class="caption">
<p>[Produto sem imagem]</p>
</div>
<?php } ?>
<div class="caption">
 <h4><strong>Descrição</strong></h4>
  <p><?php echo $prod->desc_prod; ?></p>
 </div>
</div>
<div class="col-lg-9">
<div class="panel panel-default">
	<h4 class="padd">Informações Gerais</h4>
	<div class="table-responsive">
	<table class="table table-striped table-hover table-condensed">
	<tr>
		<th>ID:</th>
		<td>
			<?php echo $prod->id; ?>
			&nbsp;
		</td>
	</tr>
	<tr>
		<th>ISBN:</th>
		<td>
			<?php echo $prod->isbn; ?>
			&nbsp;
		</td>
	</tr>
	<tr>	
		<th>Título:</th>
		<td>
			<?php echo $prod->getNome(); ?>
			&nbsp;
		</td>
	</tr>
	<tr>	
		<th>Autor:</th>
		<td>
			<?php echo $prod->autor_prod; ?>
			&nbsp;
		</td>
	</tr>
	<tr>	
		<th>Editora:</th>
		<td>
			<?php echo $prod->editora; ?>
			&nbsp;
		</td>
	</tr>
	<tr>	
		<th>Valor:</th>
		<td>
			<?php echo $prod->getValor(); ?>
			&nbsp;
		</td>
	</tr>
	<tr>	
		<th>Taxa:</th>
		<td><?php echo $prod->calculaDesconto(); ?>
			&nbsp;
		</td>
	<tr>	
		<th>Idioma:</th>
		<td>
			<?php echo $prod->idioma_prod; ?>
			&nbsp;
		</td>
	</tr>
	<tr>	
		<th>Tipo:</th>
		<td>
			<?php echo $prod->tipo; ?>
			&nbsp;
		</td>
	</tr>
	<tr>	
		<th>Quantidade:</th>
		<td>
			<?php echo $prod->qtd; ?>
			&nbsp;
		</td>
	</tr>
	<tr>	
		<th>Categoria:</th>
		<td>
			<?php echo $prod->nome; ?>
			&nbsp;
		</td>
	</tr>
	<tr>	
		<th>Campus:</th>
		<td>
			<?php echo $prod->nome_campus; ?>
			&nbsp;
		</td>
	</tr>
	<tr>	
		<th>Usado:</th>
		<td>
			<?php if ($prod->usado == 1) { ?>
				<span class="glyphicon glyphicon-thumbs-up"></span>
			<?php } else { ?>
				<span class="glyphicon glyphicon-thumbs-down"></span>
			<?php } ?> 
			&nbsp;
		</td>
	</tr>
	<tr>
	<tr>	
		<th>URL imagem:</th>
		<td>
			<a href="<?= $url_img ?><?= $prod->arq ?>" target="_blank">
				<?= $url_img ?><?= $prod->arq ?></a>
			&nbsp;
		</td>
	</tr>
	<tr>	
		<th>Ações:</th>
		<td><a href="atualiza_produto?id=<?=$prod->id;?>">
				<button class="btn btn-warning btn-sm">Editar <span class="glyphicon glyphicon-edit"></span></button>
			</a>
			<a href="deleta_produto?id=<?=$prod->id;?>">
				<button class="btn btn-danger btn-sm">Excluir <span class="glyphicon glyphicon-trash"></span></button>
			</a>
		</td>
	</tr>
	</table>
	</div>
	</div>
	<a href="index">
		<button class="btn btn-primary btn-sm">Novo produto</button>
	</a>
	<a href="lista_produtos">
		<button class="btn btn-default btn-sm">Lista produtos</button>
	</a>
</div>
</div>
	</form>
</div>
</div>
</div>

<?php require_once("footer.php"); ?>