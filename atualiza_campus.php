<?php 
	require_once("header.php");
	require_once("connection.php");
	require_once("banco_campus.php");
	$campus = selecionaCampusPorId($con, $_GET['id']);

	if(!isset($_SESSION['usuario'])) {
		header("location:form_login");
	}
?>
	<?php if(isset($_GET['atualizado'])) { ?>
		<div class="container">
			<div class="row">
			<div class="col-lg-12">
				<p class="alert alert-success">
					<span class="glyphicon glyphicon-ok-sign"></span> <?php echo $_GET['atualizado']; ?>
				</p>
			</div>
			</div>
		</div>
	<?php } ?>
<div class="container">
	<h3>Atualização do Campus #<?= $campus['id']; ?>
	</h3>
		<div class="panel panel-default well">
  <div class="panel-body">
	<form action="salva_atualizacao_campus" method="post" class="form-horizontal">
		<input type="hidden" name="id" value="<?= $campus['id']; ?>" />
		<div class="form-group">
			<label for="nome_campus" class="col-sm-2 control-label">Nome: </label>
			<div class="col-lg-4">
				<input type="text" name="nome_campus" id="nome_campus" class="form-control" value="<?php echo $campus['nome_campus']; ?>" /></div>	
		</div>
		<div class="form-group">
			<label for="logradouro" class="col-sm-2 control-label">Logradouro: </label>
			<div class="col-lg-4">
				<input type="text" name="logradouro" id="logradouro" class="form-control" value="<?php echo $campus['logradouro']; ?>" /></div>	
		</div>
	<div class="form-group">
			<label for="numero" class="col-sm-2 control-label">Número: </label>
			<div class="col-lg-4">
				<input type="text" name="numero" id="numero" class="form-control" value="<?php echo $campus['numero']; ?>" /></div>	
		</div>
		<div class="form-group">
			<label for="cep" class="col-sm-2 control-label">CEP: </label>
			<div class="col-lg-4">
				<input type="text" name="cep" id="cep" class="form-control" value="<?php echo $campus['cep']; ?>" /></div>	
		</div>

		<div class="form-group">
			<label for="cidade" class="col-sm-2 control-label">Cidade: </label>
			<div class="col-lg-4">
				<input type="text" name="cidade" id="cidade" class="form-control" value="<?php echo $campus['cidade']; ?>" /></div>	
		</div>
		<div class="form-group">
			<label for="estado" class="col-sm-2 control-label">Estado: </label>
			<div class="col-lg-4">
				<input type="text" name="estado" id="estado" class="form-control" value="<?php echo $campus['estado']; ?>" /></div>	
		</div>
		 <div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
			  <button type="submit" class="btn btn-primary">Salvar <span class="glyphicon glyphicon-floppy-disk"></span></button>
			</div>
		</div>	
	</form>
</div>
</div>
<a href="deleta_campus?id=<?=$campus['id']?>">
	<button class="btn btn-sm btn-danger">Excluir campus</button>
</a>
<a href="lista_campus">
	<button class="btn btn-sm btn-default">Lista campi</button>
</a>
</div>

<?php require_once("footer.php"); ?>
