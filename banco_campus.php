<?php
	require_once("connection.php");

	function cadastraCampus($con, $nome, $logradouro, $numero, $cep, $cidade, $estado) {
		
		$comando = "insert into campus (nome_campus, logradouro, numero, cep, cidade, estado) values ('$nome', '$logradouro', '$numero', '$cep', '$cidade', '$estado');";

		return mysqli_query($con, $comando);	
	}

	function listarCampus ($con) {

		$comando = "select * from campus;";
		$retorno = array();
		$resultado = mysqli_query($con, $comando);
	
		while($atual = mysqli_fetch_array($resultado)){
			$retorno[] = $atual;
		}
		return $retorno;				 
	}

	function selecionaCampusPorId($con, $id){

		$comando = "select * from campus where id = $id";
		$resultado = mysqli_query($con, $comando);
		$registro = mysqli_fetch_array($resultado);

		return $registro;	
	}

	function atualizaCampus($con, $id, $nome, $logradouro, $numero, $cep, $cidade, $estado) {

		$comando = "update campus set 
					nome_campus = '{$nome}',
					logradouro = '{$logradouro}', 
					numero = '{$numero}', 
					cep = '{$cep}', 
					cidade = '{$cidade}', 
					estado = '{$estado}' where id = {$id}";

		return mysqli_query($con, $comando);

	}

	function deletaCampus($con, $id) {

		$comando = "delete from campus where id = {$id}";
		return mysqli_query($con, $comando);

	}
?>
