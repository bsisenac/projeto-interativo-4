<!Doctype html>
<html>
<head> 	
	<title>Biblioteca Senac</title>
	<meta charset="utf-8"/>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css"/>
	<nav class="navbar navbar-default no-print" role="navigation">
			<div class="container">
				<div class="navbar-header">
				<a href="index" class="navbar-brand"><strong><span class="glyphicon glyphicon-book"></span> Biblioteca Senac </strong></a> 
				</div>
			
			<div class="navbar-header pull-right">
				<ul class="nav navbar-nav">
					<li><a href="contato">Suporte <span class="glyphicon glyphicon-envelope"></span></a></li>
				</ul>
				</div>
				</div>
		</nav>
</head>
<body>
