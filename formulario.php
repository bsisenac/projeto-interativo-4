	<form action="<?= $action ?>" method="post" class="form-horizontal" enctype="multipart/form-data">
		<input type="hidden" required name="id" value="<?= $prod->id; ?>" />

		<div class="form-group">
			<label for="isbn" class="col-sm-2 control-label">ISBN: </label>
			<div class="col-lg-4">
			<input type="text" required name="isbn" id="isbn" class="form-control" value="<?php echo $prod->isbn; ?>"/>
			</div>	
		</div>

		<div class="form-group">
			<label for="nome_prod" class="col-sm-2 control-label">Título: </label>
			<div class="col-lg-4">
			<input type="text" required name="nome_prod" id="nome_prod" class="form-control" value="<?php echo $prod->getNome(); ?>" />
			</div>	
		</div>

		<div class="form-group">
			<label for="autor_prod" class="col-sm-2 control-label">Autor: </label>
			<div class="col-lg-4">
			<input type="text" required name="autor_prod" id="autor_prod" class="form-control" value="<?php echo $prod->autor_prod; ?>" />
			</div>	
		</div>

		<div class="form-group">
			<label for="editora" class="col-sm-2 control-label">Editora: </label>
			<div class="col-lg-4">
			<input type="text" required name="editora" id="editora" class="form-control" value="<?php echo $prod->editora; ?>" />
			</div>	
		</div>

		<div class="form-group">
			<label for="desc_prod" class="col-sm-2 control-label">Descrição: </label>
			<div class="col-lg-4">		
			<textarea required name="desc_prod" id="desc_prod" class="form-control" rows="10"><?php echo $prod->desc_prod; ?></textarea>
			</div>
		</div>

		<div class="form-group">
			<label for="valor_prod" class="col-sm-2 control-label">Valor: </label>
			<div class="input-group col-lg-2">
      			<span class="input-group-addon">R$</span>
      				<input type="text" class="form-control col-lg-" required name ="valor_prod"  value="<?php echo $prod->getValor(); ?>" id="valor_prod" placeholder="00.00">
    		</div>
		</div>

		<div class="form-group">
			<label for="idioma_prod" class="col-sm-2 control-label">Idioma: </label>
			<div class="col-lg-2">		
				<select required name="idioma_prod" class="form-control" id="idioma_prod">
					<option value="Portugues">Português</option>
					<option value="Ingles">Inglês</option>	
					<option value="Espanhol">Espanhol</option>
				</select>
			</div>
		</div>

		<div class="form-group">
			<label for="tipo" class="col-sm-2 control-label">Tipo: </label>
			<div class="col-lg-2">		
				<select required name="tipo" class="form-control" id="tipo">
					<option value="LivroFisico">Livro Físico</option>
					<option value="Ebook">Livro Ebook</option>	
				</select>
			</div>
		</div>

		<div class="form-group">
			<label for="id_cat" class="col-sm-2 control-label">Categoria: </label>
			<div class="col-lg-2">		
			<select required name="id_cat" class="form-control" id="id_cat">
				  <?php
					$resultado = listarCategorias($con);							
					foreach($resultado as $atual) {
						$selected = $atual['id'] == $prod->id_cat ? "selected=\"selected\"":"";
						echo "<option value=\"{$atual['id']}\" $selected>{$atual['nome']}</option>";
					} 
				?>
				</select>
			</div>
		</div>

		<div class="form-group">
			<label for="id_campus" class="col-sm-2 control-label">Campus: </label>
			<div class="col-lg-2">		
			<select required name="id_campus" class="form-control" id="id_campus">
				  <?php
					$resultado = listarCampus($con);							
					foreach($resultado as $atual) {
						$selected = $atual['id'] == $prod->id_campus ? "selected=\"selected\"":"";
						echo "<option value=\"{$atual['id']}\" $selected>{$atual['nome_campus']}</option>";
					} 
				?>
				</select>
			</div>
		</div>
		
		<div class="form-group">
			<label for="qtd" class="col-sm-2 control-label">Quantidade: </label>
			<div class="col-lg-2">
			<input type="number" required name="qtd" id="qtd" class="form-control" value="<?php echo $prod->qtd; ?>" />
			</div>	
		</div>

		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
			 <div class="checkbox">
    			<label for="usado" class="control-label">
     			 <?php $checado = $prod->usado == 1 ? "checked =\"checked\"":""; ?>
     			 <input type="checkbox" <?= $checado?> value="1" name="usado" id="usado"> <strong>Usado</strong>
    			</label>
  			</div>
			</div>
		</div>

		<?php if ($action != 'salva_atualizacao') { ?>
		<div class="form-group">
			<label for="arq" class="col-sm-2 control-label"><span class="glyphicon glyphicon-picture"></span> Imagem: </label>
			<div class="col-lg-4">		
			<input type="file" name="arq" id="arq" class="form-control" value="<?php echo $prod->arq; ?>"/>
			</div>
		</div>
		<?php } ?>
		 <div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
			  <button type="submit" class="btn btn-primary"> <?= $textobotao ?> <span class="glyphicon glyphicon-floppy-disk"></span></button>
			</div>
		</div>	
	</form>