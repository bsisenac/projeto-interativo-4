<?php 

	class Usuario {
		private $email;
		private $senha;
		private $nome;

		public function getNome() {
				return $this->nome;		
		}

		public function setNome($novoNome) {	
			$this->nome = $novoNome;
		}

		public function getEmail() {
				return $this->email;		
		}

		public function setEmail($novoEmail) {	
			$this->email = $novoEmail;
		}

		public function getSenha() {
			return $this->senha;		
		}

		public function setSenha($novoSenha) {	
			$this->senha = $novoSenha;
		}
	}

?>

