<?php 

	abstract class Produto {
		public $id; 
		private $nome_prod; 
		public $autor_prod; 
		protected $valor_prod; 
		public $desc_prod;
		public $idioma_prod;  
		public $id_cat;
		public $id_campus;
		public $usado;
		public $arq;
		public $editora;
		public $qtd;
	
		function __construct ($nomeprod="", $valorprod=""){
			$this->nome_prod = $nomeprod;
			$this->valor_prod = $valorprod;
			$this->desc_prod = "";
			$this->idioma_prod = "";
			$this->id_cat = "";
			$this->id_campus = "";
			$this->usado = 0;
			$this->arq = "";
			$this->isbn = "";
			$this->tipo = "";
			$this->editora = "";
			$this->qtd = "";
		}
		
		function __tostring (){
			return "produto:{$this->nome_prod}, {$this->valor_prod}";
		}

		public abstract function calculaDesconto ($valorDesconto);

		public function getNome(){
			return $this->nome_prod;
		}

		public function getValorFormatado() {
			return str_replace(".", ",", number_format($this->valor_prod,2));		
		}

		public function getValor() {
			return $this->valor_prod;		
		}
	}
?>
