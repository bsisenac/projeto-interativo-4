<?php 
	class ProdutoDAO {

		private $con;

		function __construct ($conexao) {
			$this->con = $conexao;
			
		}

		public function cadastraProduto($produto) {

			$comando = "insert into produtos (nome_prod, autor_prod, valor_prod, desc_prod, idioma_prod, editora, id_cat, id_campus, usado, arq, isbn, tipo, qtd) 
						values ('{$produto->getNome()}', '{$produto->autor_prod}', '{$produto->getValor()}', '{$produto->desc_prod}', '{$produto->idioma_prod}', '{$produto->editora}', '{$produto->id_cat}', '{$produto->id_campus}', '{$produto->usado}', '{$produto->arq}', '{$produto->isbn}', '{$produto->tipo}', '{$produto->qtd}');";

			return mysqli_query($this->con, $comando);	
		}

		public function selecionaProdutos() {

			$comando = "select produtos.id, nome_prod, autor_prod, valor_prod, desc_prod, idioma_prod, editora, usado, arq, isbn, tipo, qtd, nome, nome_campus from produtos inner join categorias on id_cat = categorias.id inner join campus on id_campus = campus.id order by produtos.id DESC";

			$retorno = array();
			$resultado = mysqli_query($this->con, $comando);

			while ($atual = mysqli_fetch_array($resultado)) {

					$produto = new $atual['tipo']($atual['nome_prod'], $atual['valor_prod']);
					$produto->id = $atual['id'];
					$produto->autor_prod = $atual['autor_prod'];
					$produto->desc_prod = $atual['desc_prod'];
					$produto->idioma_prod = $atual['idioma_prod'];
					$produto->nome = $atual['nome'];
					$produto->usado = $atual['usado'];
					$produto->arq = $atual['arq'];
					$produto->isbn = $atual['isbn'];
					$produto->tipo = $atual['tipo'];
					$produto->editora = $atual['editora'];
					$produto->qtd = $atual['qtd'];
					$produto->nome_campus = $atual['nome_campus'];
			
				$retorno[] = $produto;		
			}

			return $retorno;

		}
	
		public function selecionaProdutoPorId($id){

			$comando = "select * from produtos where id = $id";
			$resultado = mysqli_query($this->con, $comando);
			$registro = mysqli_fetch_array($resultado);

			$retorno = new $registro['tipo']($registro['nome_prod'], $registro['valor_prod']);
			$retorno->id = $id;
			$retorno->isbn = $registro['isbn'];
			$retorno->autor_prod = $registro['autor_prod'];
			$retorno->desc_prod = $registro['desc_prod'];
			$retorno->idioma_prod = $registro['idioma_prod'];
			$retorno->id_cat = $registro['id_cat'];
			$retorno->id_campus = $registro['id_campus'];	
			$retorno->usado = $registro['usado'];
			$retorno->arq = $registro['arq'];
			$retorno->tipo = $registro['tipo'];
			$retorno->qtd = $registro['qtd'];
			$retorno->editora = $registro['editora'];

			return $retorno;	
		}

		public function visualizaProdutoPorId($id){

			$comando = "select produtos.id, nome_prod, autor_prod, valor_prod, desc_prod, idioma_prod, editora, usado, arq, isbn, tipo, qtd, nome, nome_campus from produtos inner join categorias on id_cat = categorias.id inner join campus on id_campus = campus.id where produtos.id = $id order by produtos.id DESC";
			$resultado = mysqli_query($this->con, $comando);
			$registro = mysqli_fetch_array($resultado);

			$retorno = new $registro['tipo']($registro['nome_prod'], $registro['valor_prod']);
			$retorno->id = $id;
			$retorno->isbn = $registro['isbn'];
			$retorno->autor_prod = $registro['autor_prod'];
			$retorno->desc_prod = $registro['desc_prod'];
			$retorno->idioma_prod = $registro['idioma_prod'];
			$retorno->nome = $registro['nome'];
			$retorno->nome_campus = $registro['nome_campus'];	
			$retorno->usado = $registro['usado'];
			$retorno->arq = $registro['arq'];
			$retorno->tipo = $registro['tipo'];
			$retorno->editora = $registro['editora'];
			$retorno->qtd = $registro['qtd'];

			return $retorno;	

		}

		public function atualizaProduto($produto) {

			$comando = "update produtos set 
						nome_prod = '{$produto->getNome()}',
						autor_prod = '{$produto->autor_prod}', 
						valor_prod = '{$produto->getValor()}',
						desc_prod = '{$produto->desc_prod}',
						idioma_prod = '{$produto->idioma_prod}',
						autor_prod = '{$produto->autor_prod}',
						id_cat = '{$produto->id_cat}}',
						id_campus = '{$produto->id_campus}}',
						usado = '{$produto->usado}',
						isbn = '{$produto->isbn}',
						editora = '{$produto->editora}',
						qtd = '{$produto->qtd}',
						tipo = '{$produto->tipo}' where id = '{$produto->id}'";

			return mysqli_query($this->con, $comando) or die(mysqli_error($comando));
		}
	
		public function deletaProduto ($id) {

			$comando = "delete from produtos where id = {$id}";
			return mysqli_query($this->con, $comando);
		}
	}
	
?>
