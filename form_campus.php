<?php 
	require_once("header.php");
	require_once("connection.php");
	require_once("banco_campus.php");

	if(!isset($_SESSION['usuario'])) {
		header("location:form_login");
	}
?>
	<?php if(isset($_GET['msg'])) { ?>
		<div class="container">
			<div class="row">
			<div class="col-lg-12">
				<p class="alert alert-success">
					<span class="glyphicon glyphicon-ok-sign"></span> <?php echo $_GET['msg']; ?>
				</p>
			</div>
			</div>
		</div>
	<?php } ?>
<div class="container">
	<h3>Cadastro de Campus
	<a href="lista_campus">
		<button class="btn btn-primary pull-right">Lista campi</button>
	</a>
	</h3>
		<div class="panel panel-default well">
  <div class="panel-body">
	<form action="cadastro_campus" method="post" class="form-horizontal">
		<div class="form-group">
			<label for="nome_campus" class="col-sm-2 control-label">Nome: </label>
			<div class="col-lg-4">
			<input type="text" name="nome_campus" id="nome_campus" required="required" class="form-control"/>
			</div>	
		</div>
		<div class="form-group">
			<label for="logradouro" class="col-sm-2 control-label">Logradouro: </label>
			<div class="col-lg-4">
			<input type="text" name="logradouro" id="logradouro" required="required" class="form-control"/>
			</div>	
		</div>
		<div class="form-group">
			<label for="numero" class="col-sm-2 control-label">Número: </label>
			<div class="col-lg-4">
			<input type="text" name="numero" id="numero" required="required" class="form-control"/>
			</div>	
		</div>
		<div class="form-group">
			<label for="cep" class="col-sm-2 control-label">CEP: </label>
			<div class="col-lg-4">
			<input type="text" name="cep" id="cep" required="required" class="form-control" placeholder="0000-000"/>
			</div>	
		</div>
		<div class="form-group">
			<label for="cidade" class="col-sm-2 control-label">Cidade: </label>
			<div class="col-lg-4">
			<input type="text" name="cidade" id="cidade" required="required" class="form-control"/>
			</div>	
		</div>
		<div class="form-group">
			<label for="estado" class="col-sm-2 control-label">Estado: </label>
			<div class="col-lg-4">
			<input type="text" name="estado" id="estado" required="required" class="form-control"/>
			</div>	
		</div>
		 <div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
			  <button type="submit" class="btn btn-primary">Salvar <span class="glyphicon glyphicon-floppy-disk"></span></button>
			</div>
		</div>	
	</form>
</div>
</div>
</div>

<?php require_once("footer.php"); ?>
