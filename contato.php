<?php 
	require_once("header.php");

?>

<?php if(isset($_GET['enviado'])) { ?>
		<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<p class="alert alert-success">
					<span class="glyphicon glyphicon-info-sign"></span> <?php echo $_GET['enviado']; ?>
				</p>
			</div>
		</div>
</div>
	<?php } ?>
<div class="container">
	<h3>Contate o Suporte</h3>
	<div class="panel panel-default well">
  <div class="panel-body">
	
	<form action="envia_email" method="post" class="form-horizontal">
		<div class="form-group">
			<label for="nome" class="col-sm-2 control-label">Nome: </label>
			<div class="col-lg-4">
			<input type="text" name="nome" id="nome" class="form-control" required placeholder="seu nome..."/>
			</div>	
		</div>

		<div class="form-group">
			<label for="email" class="col-sm-2 control-label">E-mail: </label>
			<div class="col-lg-4">
			<input type="text" name="email" class="form-control" id="email" required placeholder="seu e-mail..."/>
			</div>	
		</div>

		<div class="form-group">
			<label for="assunto" class="col-sm-2 control-label">Assunto: </label>
			<div class="col-lg-4">
			<input type="text" name="assunto" class="form-control" id="assunto" required placeholder="assunto do email..."/>
			</div>	
		</div>

		<div class="form-group">
			<label for="msg" class="col-sm-2 control-label">Mensagem: </label>
			<div class="col-lg-4">
			<textarea name="msg" class="form-control" rows="10" id="msg" required placeholder="Descreva o que deseja tratar com o suporte..."></textarea>
			</div>	
		</div>

		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
			  <button type="submit" class="btn btn-primary">Enviar <span class="glyphicon glyphicon-send"></span></button>
			  <button type="reset" class="btn btn-default">Limpar </button>
			</div>
		</div>	
</form>

</div>
</div>
</div>
<?php require_once("footer.php"); ?>
