<?php 
	require_once("connection.php");
	require_once("banco_usuario.php");
	include('header.php');

	if(!isset($_SESSION['usuario'])) {
		header("location:form_login");
	}
?>

<?php if(isset($_GET['msg'])) { ?>
		<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<p class="alert alert-success">
					<span class="glyphicon glyphicon-remove-sign"></span> <?php echo $_GET['msg']; ?>
				</p>
			</div>
		</div>
</div>
	<?php } ?>

<?php if(isset($_GET['atualizado'])) { ?>
		<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<p class="alert alert-success">
					<span class="glyphicon glyphicon-info-sign"></span> <?php echo $_GET['atualizado']; ?>
				</p>
			</div>
		</div>
</div>
	<?php } ?>

<div class="container">
<h3>Lista de Usuários
	<a href="form_usuario">
		<button class="btn btn-sm btn-primary ">Novo usuário
			<span class="glyphicon glyphicon-plus"></span>
		</button>
	</a>
</h3> 
<div class="table-responsive">
<table class="table table-hover panel panel-default">
	<thead class="well panel-body">
 		<tr>
			<th>ID</th>
			<th>Nome</th>
			<th>Email</th>
			<th>Senha</th>
			<th>Ações</th>
		</tr>		
	</thead>

	<tbody>
		<?php 
		$resultado = listarUsuarios($con); 
		foreach($resultado as $atual) {		
			echo
				"<tr>
					<td>{$atual['id']}</td>
					<td>{$atual['nome']}</td>
					<td>{$atual['email']}</td>
					<td>{$atual['senha']}</td>
					<td>
						<a href=\"atualiza_usuario?id={$atual['id']}\">
						<button class=\"btn btn-sm btn-warning\">Editar <span class=\"glyphicon glyphicon-edit\"></span></button>
						</a>
						<a href=\"deleta_usuario?id={$atual['id']}\">
						<button class=\"btn btn-sm btn-danger\">Excluir <span class=\"glyphicon glyphicon-trash\"></span></button>
						</a>
					</td>
				</tr>"; 
		}
		?>
	</tbody>
</table>
</div>
</div>

<?php include('footer.php'); ?>
