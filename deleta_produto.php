<?php
	include('connection.php');
	include('autoload.php');

	$id = $_GET['id'];

	$dao = new ProdutoDAO($con);

	$deuCerto = $dao->deletaProduto($id);

	if($deuCerto == true) {
		$url = "lista_produtos?msg=Produto excluído com sucesso!";
	} else {
		$url = "lista_produtos?msg=Produto não foi excluído!";	
	}

	header("location: {$url}");

?>
