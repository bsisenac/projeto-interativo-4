create table produtos (
	id int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
	nome_prod varchar(255) NOT NULL,
	valor_prod decimal(10,2) NOT NULL,
	desc_prod text NOT NULL,
	id_cat int(11) NOT NULL
);

create table categorias (
	id int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
	nome varchar(255) NOT NULL
);

create table usuarios (
	id int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
	email varchar(255) NOT NULL,
	senha varchar(255) NOT NULL
);
