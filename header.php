<!Doctype html>
<html>
<head> 	
	<title>Biblioteca Senac</title>
	<meta charset="utf-8"/>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css"/>
	<nav class="navbar navbar-default no-print" role="navigation">
			<div class="container">
				<div class="navbar-header">
				<a href="lista_produtos" class="navbar-brand"><strong><span class="glyphicon glyphicon-book"></span> Biblioteca Senac </strong></a> 
				</div>
				<ul class="nav navbar-nav">
					<li><a href="lista_produtos"> Produtos <span class="glyphicon glyphicon-file"></span> </a></li>
					<li><a href="lista_categorias"> Categorias <span class="glyphicon glyphicon-tags"></span></a></li>
					<li><a href="lista_usuarios"> Usuários <span class="glyphicon glyphicon-user"></span> </a></li>
					<li><a href="lista_campus"> Campi <span class="glyphicon glyphicon-home"></span> </a></li>
					<li><a href="contato"> Suporte <span class="glyphicon glyphicon-envelope"></span> </a></li>
				</ul>
				<div class="navbar-header pull-right">
				<ul class="nav navbar-nav">
					<li><a href="logout"><strong>Logout <span class="glyphicon glyphicon-log-out"></span></strong></a></li>
				</ul>
				</div>
			</div>
		</nav>
</head>
<body>
