<?php
	require_once("connection.php");

	function cadastraCategoria($con, $nome) {
		
		$comando = "insert into categorias (nome) values ('$nome');";

		return mysqli_query($con, $comando);	
	}

	function listarCategorias ($con) {

		$comando = "select id, nome from categorias;";
		$retorno = array();
		$resultado = mysqli_query($con, $comando);
	
		while($atual = mysqli_fetch_array($resultado)){
			$retorno[] = $atual;
		}
		return $retorno;				 
	}

	function selecionaCategoriaPorId($con, $id){

		$comando = "select * from categorias where id = $id";
		$resultado = mysqli_query($con, $comando);
		$registro = mysqli_fetch_array($resultado);

		return $registro;	
	}

	function atualizaCategoria($con, $id, $nome) {

		$comando = "update categorias set 
					nome = '{$nome}' where id = {$id}";

		return mysqli_query($con, $comando);

	}

	function deletaCategoria ($con, $id) {

		$comando = "delete from categorias where id = {$id}";
		return mysqli_query($con, $comando);

	}
?>
