<?php 

	include("header.php");
	require_once("connection.php");
	require_once("banco_campus.php");

	$nome = $_POST['nome_campus'];
	$logradouro = $_POST['logradouro'];
	$numero = $_POST['numero'];
	$cep = $_POST['cep'];
	$cidade = $_POST['cidade'];
	$estado = $_POST['estado'];

	$deuCerto = cadastraCampus($con, $nome, $logradouro, $numero, $cep, $cidade, $estado);

	if($deuCerto == true) {
		$url = "form_campus?msg=Campus cadastrado com sucesso!";
	} else {
		$url = "form_campus?msg=Campus não foi cadastrado!";	
	}
	
	header("location: {$url}");

	include("footer.php");

?>

