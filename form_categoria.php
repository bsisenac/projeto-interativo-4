<?php 
	require_once("header.php");
	require_once("connection.php");
	require_once("banco_categorias.php");

	if(!isset($_SESSION['usuario'])) {
		header("location:form_login");
	}
?>
	<?php if(isset($_GET['msg'])) { ?>
		<div class="container">
			<div class="row">
			<div class="col-lg-12">
				<p class="alert alert-success">
					<span class="glyphicon glyphicon-ok-sign"></span> <?php echo $_GET['msg']; ?>
				</p>
			</div>
			</div>
		</div>
	<?php } ?>
<div class="container">
	<h3>Cadastro de Categorias
	<a href="lista_categorias">
		<button class="btn btn-primary pull-right">Lista categorias</button>
	</a>
	</h3>
		<div class="panel panel-default well">
  <div class="panel-body">
	<form action="cadastro_categoria" method="post" class="form-horizontal">
		<div class="form-group">
			<label for="nome" class="col-sm-2 control-label">Nome: </label>
			<div class="col-lg-4">
			<input type="text" name="nome" id="nome" required="required" class="form-control"/>
			</div>	
		</div>
		
		 <div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
			  <button type="submit" class="btn btn-primary">Salvar <span class="glyphicon glyphicon-floppy-disk"></span></button>
			</div>
		</div>	
	</form>
</div>
</div>
</div>

<?php require_once("footer.php"); ?>
