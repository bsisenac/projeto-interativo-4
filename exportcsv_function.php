<?php 

    function exportaCSV($table, $filename = 'lista_produtos.csv') {

    $csv_terminated = "\n";
    $csv_separator = ",";
    $csv_enclosed = '"';
    $csv_escaped = "\\";

    $sql_query = "select $table.id as 'ID', nome_prod as 'Titulo', autor_prod as 'Autor', valor_prod as 'Valor', desc_prod as 'Descrição', idioma_prod as 'Idioma', editora as 'Editora', isbn as 'ISBN', tipo as 'Tipo', nome as 'Categoria', nome_campus as 'Campus', usado, qtd as 'Quantidade' from $table inner join categorias on id_cat = categorias.id inner join campus on id_campus = campus.id order by $table.id";
 
    $result = mysql_query($sql_query);
    $fields_cnt = mysql_num_fields($result);
 
    $schema_insert = '';
 
    for ($i = 0; $i < $fields_cnt; $i++)
    {
        $l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,
            stripslashes(mysql_field_name($result, $i))) . $csv_enclosed;
        $schema_insert .= $l;
        $schema_insert .= $csv_separator;
    }
 
    $out = trim(substr($schema_insert, 0, -1));
    $out .= $csv_terminated;
 
    while ($row = mysql_fetch_array($result))
    {
        $schema_insert = '';
        for ($j = 0; $j < $fields_cnt; $j++)
        {
            if ($row[$j] == '0' || $row[$j] != '')
            {
 
                if ($csv_enclosed == '')
                {
                    $schema_insert .= $row[$j];
                } else
                {
                    $schema_insert .= $csv_enclosed . 
                    str_replace($csv_enclosed, $csv_escaped . $csv_enclosed, $row[$j]) . $csv_enclosed;
                }
            } else
            {
                $schema_insert .= '';
            }
 
            if ($j < $fields_cnt - 1)
            {
                $schema_insert .= $csv_separator;
            }
        }
 
        $out .= $schema_insert;
        $out .= $csv_terminated;
    }
 
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Length: " . strlen($out));
    header("Content-type: text/x-csv");
    //header("Content-type: text/csv");
    //header("Content-type: application/csv");
    header("Content-Disposition: attachment; filename=$filename");
    echo $out;
    exit;
	}
?>