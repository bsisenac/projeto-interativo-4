<?php 
	require_once("header.php");
	require_once("connection.php");
	require_once("autoload.php");
	require_once("banco_categorias.php");
	require_once("banco_campus.php");
	$action = "salva_atualizacao";
	$textobotao = "Alterar";
	$dao = new ProdutoDAO($con);
	$prod = $dao->selecionaProdutoPorId($_GET['id']);

?>
	
<div class="container">
	<h3>Atualização do Poduto #<?= $prod->id ?>
	</h3>
	<div class="panel panel-default well">
	<div class="panel-body">
		<?php include('formulario.php'); ?>
	</form>
</div>
</div>
<a href="deleta_produto?id=<?=$prod->id;?>">
	<button class="btn btn-sm btn-danger">Excluir produto</button>
</a>
<a href="lista_produtos">
	<button class="btn btn-sm btn-default">Lista produtos</button>
</a>
</div>

<?php require_once("footer.php"); ?>
