<?php 
	require_once("connection.php");
	require_once("banco_categorias.php");

	$id = $_POST['id'];
	$nome = $_POST['nome'];
		
	$deuCerto = atualizaCategoria($con, $id, $nome);

	if($deuCerto == true) {
		$url = "lista_categorias?atualizado=A Categoria foi atualizada com sucesso!";
	} else {
		$url = "lista_categorias?atualizado=A Categoria não foi atualizada!";	
	}
	
	header("location: {$url}");

	include("footer.php");

?>