<?php
	include('connection.php');
	include('banco_campus.php');

	$id = $_GET['id'];

	$deuCerto = deletaCampus($con, $id);

	if($deuCerto == true) {
		$url = "lista_campus?msg=Campus deletado com sucesso!";
	} else {
		$url = "lista_campus?msg=Campus não foi deletado!";	
	}

	header("location: {$url}");

?>