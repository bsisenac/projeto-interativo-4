<?php 
	require_once("header.php");
	require_once("connection.php");
	require_once("autoload.php");
	require_once("banco_categorias.php");
	require_once("banco_campus.php");

	if(!isset($_SESSION['usuario'])) {
		header("location:form_login");
	}

	$action = "cadastro_produto";
	$textobotao = "Salvar";


	$prod = new LivroFisico();

?>
	<?php if(isset($_GET['msg'])) { ?>
		<div class="container">
			<div class="row">
			<div class="col-lg-12">
				<p class="alert alert-success">
					<span class="glyphicon glyphicon-ok-sign"></span> <?php echo $_GET['msg']; ?>
				</p>
			</div>
		</div>
		</div>
	<?php } ?>
<div class="container">
	<h3>Cadastro de Podutos
	<a href="lista_produtos">
		<button class="btn btn-primary pull-right">Lista produtos</button>
	</a>
	</h3> 
	
	<div class="panel panel-default well">
  <div class="panel-body">
	<?php include('formulario.php'); ?>
</div>
</div>
</div>

<?php require_once("footer.php"); ?>
