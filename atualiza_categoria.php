<?php 
	require_once("header.php");
	require_once("connection.php");
	require_once("banco_categorias.php");
	$cat = selecionaCategoriaPorId($con, $_GET['id']);

	if(!isset($_SESSION['usuario'])) {
		header("location:form_login");
	}
?>
	<?php if(isset($_GET['atualizado'])) { ?>
		<div class="container">
			<div class="row">
			<div class="col-lg-12">
				<p class="alert alert-success">
					<span class="glyphicon glyphicon-ok-sign"></span> <?php echo $_GET['atualizado']; ?>
				</p>
			</div>
			</div>
		</div>
	<?php } ?>
<div class="container">
	<h3>Atualização da Categoria #<?= $cat['id']; ?>
	</h3>
		<div class="panel panel-default well">
  <div class="panel-body">
	<form action="salva_atualizacao_cat" method="post" class="form-horizontal">
		<input type="hidden" name="id" value="<?= $cat['id']; ?>" />
		<div class="form-group">
			<label for="nome" class="col-sm-2 control-label">Nome: </label>
			<div class="col-lg-4">
				<input type="text" name="nome" id="nome" class="form-control" value="<?php echo $cat['nome']; ?>" /></div>	
		</div>
		
		 <div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
			  <button type="submit" class="btn btn-primary">Salvar <span class="glyphicon glyphicon-floppy-disk"></span></button>
			</div>
		</div>	
	</form>
</div>
</div>
<a href="deleta_categoria?id=<?=$cat['id']?>">
	<button class="btn btn-sm btn-danger">Excluir categoria</button>
</a>
<a href="lista_categorias">
	<button class="btn btn-sm btn-default">Lista categorias</button>
</a>
</div>

<?php require_once("footer.php"); ?>
