<?php 
	require_once("connection.php");
	require_once("autoload.php");
	include('header.php');

	if(!isset($_SESSION['usuario'])) {
		header("location:form_login");
	}
?>

<?php include('busca_produto.php'); ?>

<?php if(isset($_GET['msg'])) { ?>
		<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<p class="alert alert-success">
					<span class="glyphicon glyphicon-remove-sign"></span> <?php echo $_GET['msg']; ?>
				</p>
			</div>
		</div>
</div>
	<?php } ?>

<?php if(isset($_GET['csv'])) { ?>
		<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<p class="alert alert-info">
					<span class="glyphicon glyphicon-info-sign"></span> <?php echo $_GET['csv']; ?>
				</p>
			</div>
		</div>
</div>
	<?php } ?>

<?php if(isset($_GET['atualizado'])) { ?>
		<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<p class="alert alert-success">
					<span class="glyphicon glyphicon-info-sign"></span> <?php echo $_GET['atualizado']; ?>
				</p>
			</div>
		</div>
</div>
	<?php } ?>


<div class="container">
<h3>Lista de Produtos 
	<a href="index">
		<button class="btn btn-primary btn-sm">Novo produto 
			<span class="glyphicon glyphicon-plus"></span>
		</button>
	</a>
	<a href="exporta_csv">
		<button class="btn btn-success btn-sm">Exportar CSV 
			<span class="glyphicon glyphicon-download-alt"></span>
		</button>
	</a>
</h3> 
		
<div class="table-responsive">
<table class="table table-hover panel panel-default">
	<thead class="well panel-body">
 		<tr>
			<th>ID</th>
			<th>Título</th>
			<th>Autor</th>
			<th>Taxa</th>
			<th>Categoria</th>
			<th>Tipo</th>
			<th>Campus</th>
			<th>Quantidade</th>
			<th>Usado</th>
			<th>Ações</th>
		</tr>		
	</thead>

	<tbody>
		<?php
		$dao = new ProdutoDAO($con); 
		$resultado = $dao->selecionaProdutos($con); 
		//var_dump($resultado); exit;
		foreach($resultado as $atual) {	
				echo
					"<tr>
						<td>{$atual->id}</td>
						<td>{$atual->getNome()}</td>
						<td>{$atual->autor_prod}</td>
						<td>{$atual->calculaDesconto()}</td>
						<td>{$atual->nome}</td>
						<td>{$atual->tipo}</td>
						<td>{$atual->nome_campus}</td>
						<td>{$atual->qtd}</td>
						<td>"?><?php if ($atual->usado == 1) { ?>
							<span class="glyphicon glyphicon-thumbs-up"></span>	
							<?php } else { ?>
							<span class="glyphicon glyphicon-thumbs-down"></span>
						<?php } ?> 
						<?php echo "</td>
						<td><a href=\"ver_produto?id={$atual->id}\">
							<button class=\"btn btn-sm btn-info\">Ver <span class=\"glyphicon glyphicon-eye-open\"></span></button>
							</a>
							<a href=\"atualiza_produto?id={$atual->id}\">
							<button class=\"btn btn-sm btn-warning\">Editar <span class=\"glyphicon glyphicon-edit\"></span></button>
							</a>
							<a href=\"deleta_produto?id={$atual->id}\">
							<button class=\"btn btn-sm btn-danger\">Excluir <span class=\"glyphicon glyphicon-trash\"></span></button>
							</a>
						</td>
					</tr>";
		}
		?>
	</tbody>
</table>
</div>
</div>

<?php include('footer.php'); ?>
