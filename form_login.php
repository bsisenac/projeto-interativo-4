<?php 
	require_once("header-login.php");
	require_once("connection.php");
	require_once('banco_usuario.php');
?>
<?php if(isset($_GET['login'])) { ?>
		<div class="container">
			<div class="row">
			<div class="col-lg-12">
				<p class="alert alert-danger">
					<span class="glyphicon glyphicon-ban-circle"></span> <?php echo $_GET['login']; ?>
				</p>
			</div>
		</div>
		</div>
	<?php } ?>	
<div class="container">
	<h3>Sistema de Controle de Produtos</h3>
	<div class="panel panel-default well">
	<h4>Acesso ao Sistema</h4>
	<hr>
  <div class="panel-body ">
	<form action="autentica" method="post" class="form-horizontal">
		<div class="form-group">
			<label for="email" class="col-sm-2 control-label">E-mail: </label>
			<div class="input-group col-lg-4">
				<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
			<input type="text" name="email" id="email" required="required" class="form-control"/>
			</div>	
		</div>

		<div class="form-group">
			<label for="senha" class="col-sm-2 control-label">Senha: </label>
			<div class="input-group col-lg-4">
			<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
			<input type="password" name="senha" id="senha" required="required" class="form-control"/>
			</div>	
		</div>

		 <div class="form-group">
			<div class="col-sm-offset-2">
			  <button type="submit" class="btn btn-primary ">Entrar <span class="glyphicon glyphicon-log-in"></span></button>
			</div>
		</div>	
	</form>
</div>
</div>
</div>

<?php require_once("footer.php"); ?>
