<?php 
	require_once("connection.php");
	require_once("banco_usuario.php");

	$id = $_POST['id'];
	$nome = $_POST['nome'];
	$email = $_POST['email'];
	$senha = $_POST['senha'];
		
	$deuCerto = atualizaUsuario($con, $id, $nome, $email, $senha);

	if($deuCerto == true) {
		$url = "lista_usuarios?atualizado=O Usuário foi atualizado com sucesso!";
	} else {
		$url = "lista_usuarios?atualizado=O Usuário não foi atualizado!";	
	}
	
	header("location: {$url}");

	include("footer.php");

?>