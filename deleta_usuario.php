<?php
	include('connection.php');
	include('banco_usuario.php');

	$id = $_GET['id'];

	$deuCerto = deletaUsuario($con, $id);

	if($deuCerto == true) {
		$url = "lista_usuarios?msg=Usuário deletado com sucesso!";
	} else {
		$url = "lista_usuarios?msg=Usuário não foi deletado!";	
	}

	header("location: {$url}");

?>