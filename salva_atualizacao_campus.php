<?php 
	require_once("connection.php");
	require_once("banco_campus.php");

	$id = $_POST['id'];
	$nome = $_POST['nome_campus'];
	$logradouro = $_POST['logradouro'];
	$numero = $_POST['numero'];
	$cep = $_POST['cep'];
	$cidade = $_POST['cidade'];
	$estado = $_POST['estado'];
		
	$deuCerto = atualizaCampus($con, $id, $nome, $logradouro, $numero, $cep, $cidade, $estado);

	if($deuCerto == true) {
		$url = "lista_campus?atualizado=O Campus foi atualizado com sucesso!";
	} else {
		$url = "lista_campus?atualizado=O Campus não foi atualizado!";	
	}
	
	header("location: {$url}");

	include("footer.php");

?>