<?php
require_once("autoload.php");
require_once('connection.php');
require_once('banco_usuario.php');
	
	$usuario = new Usuario();
	$usuario->setEmail($_POST['email']);
	$usuario->setSenha($_POST['senha']);

	$valido = validaUsuario($con, $usuario);
	
	$email = mysqli_real_escape_string($con, $usuario->getEmail());
	$senha = mysqli_real_escape_string($con, $usuario->getSenha());

	if($valido){
		$_SESSION['usuario'] = $usuario->getEmail();
		header("location:lista_produtos");
	} else {
		$url = "form_login?login=Usuário ou senha incorretos! Por favor, tente novamente.";	
		header("location: {$url}");
	}
?>
