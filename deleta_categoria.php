<?php
	include('connection.php');
	include('banco_categorias.php');

	$id = $_GET['id'];

	$deuCerto = deletaCategoria($con, $id);

	if($deuCerto == true) {
		$url = "lista_categorias?msg=Categoria deletada com sucesso!";
	} else {
		$url = "lista_categorias?msg=Categoria não foi deletada!";	
	}

	header("location: {$url}");

?>